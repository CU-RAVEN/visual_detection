%% Housekeeping
close all
clear all

%% Define Simulation Parameters
flight_sec = 15*60; %[s] Total flight time
timestep = .1;      %[s]
%Determine UAV orbit
[t_uav, x_uav, y_uav, z_uav, heading_uav] = orbit_model( ...
    7.07106, ...   %The radius of the UAV orbit [m]
    7.07106, ...   %The altitude of the orbit [m]
    5, ...         %The flight speed [m/s]
    flight_sec,... %The total time of flight [s]
    timestep ...   %Timestep [s]
    );
%Define UGV 'orbit'
[t_ugv, x_ugv, y_ugv, z_ugv, heading_ugv] = orbit_model( ...
    0.000000001, ...            %The radius of the UGV orbit [m]
    0, ...                      %The altitude of the orbit [m]
    0.00000000000000001, ...    %The UGV speed speed [m/s]
    flight_sec,...              %The total time of flight [s]
    timestep ...                %Timestep [s]
    );
%Extremely low in order to simulate stationary


%% Add in errors
%Preallocate memory
x_uav_err = linspace(0,0,length(t_uav));
y_uav_err = linspace(0,0,length(t_uav));
x_uav_err_rtk = linspace(0,0,length(t_uav));
y_uav_err_rtk = linspace(0,0,length(t_uav));
z_uav_err = linspace(0,0,length(t_uav));
heading_uav_err = linspace(0,0,length(t_uav));
for i = 1:length(t_uav)
    %Add error in UAV position (GPS)
    %GPS error is 2 meters at 2 sigma
    %Based on XXXXXX
    %2.5 m at 50%
    %0.025 at 50% with RTK
    x_uav_err(i) = normrnd(0,2.5/0.6745);
    x_uav_err_rtk(i) = normrnd(0,0.025/0.6745);
    
    y_uav_err(i) = normrnd(0,2/0.6745);
    y_uav_err_rtk(i) = normrnd(0,0.025/0.6745);
    
    
    %Add error in Altitude
    %Error in altitude is 10 cm at 3 sigma
    %http://www.te.com/usa-en/product-CAT-BLPS0036.html
    z_uav_err(i) = normrnd(0,0.1/3);
    
    %Add error in bearing
    heading_uav_err(i) = heading_uav(i)+normrnd(0,1/2);
    
end

%% Plot Error
% figure
% hold on
% plot(x_uav_err,'Linewidth',2)
% plot(x_uav_err_rtk,'Linewidth',2)
% hold off
%
% figure
% hold on
% plot(y_uav_err,'Linewidth',2)
% plot(y_uav_err_rtk,'Linewidth',2)
% hold off
%
% figure
% hold on
% plot(z_uav_err,'Linewidth',2)
% hold off
%
% figure
% hold on
% plot(heading_uav_err,'Linewidth',2)
% hold off

%% Plot diagram

%Prealocate AOV_angles Array
AOV_angle = linspace(0,0,length(t_uav));
AOV_angle_rtk = linspace(0,0,length(t_uav));
%Create new figure
figure

%Define latency in seconds
latency = 0.5; %[s]

for i = 1:length(t_uav)
    
    %% Define UAV errors
    x_err_uav = x_uav_err_rtk(i); %x_uav_err(i); %As determined by the GPS error
    y_err_uav = y_uav_err_rtk(i); %y_uav_err(i); %As determined by the GPS error
    z_err_uav = z_uav_err(i);
    
    
    %% Create error ellipsoid for UAV;
    [a_uav,b_uav,c_uav] = ellipsoid(x_uav(i + latency/timestep), ...  % True X position
        y_uav(i + latency/timestep), ...  % True Y position
        z_uav(i + latency/timestep), ...  % True Altitude
        x_err_uav, ...  %Error as defined above
        y_err_uav, ...  %Error as defined above
        z_err_uav);     %Error as defined above
    %Plot UAV possible location ellipsoid
    surf(a_uav,b_uav,c_uav)
    %Set axes immediately
    axis(2*[min(x_uav),max(x_uav),min(y_uav),max(y_uav),0,max(z_uav)]);
    hold on; %
    
    %% Plot true cone/vector showing UGV to UAV. (Accounting for latency
    [~,~,~] = cone([x_ugv(i),y_ugv(i),z_ugv(i)], ... %Location of UGV
        [x_uav(i+ latency/timestep), ...
        y_uav(i+ latency/timestep),...
        z_uav(i+ latency/timestep)],...%Location of UAV
        [0,max([max(x_err_uav),max(y_err_uav),z_err_uav])], ... %Max radius
        100,'b',0,0); %100 segments, Blue, Open Cone, No Lines
    
    %% Annotations
    %Annotate where the UAV is
    text(x_uav(i+ latency/timestep),...
        y_uav(i+ latency/timestep),...
        z_uav(i+ latency/timestep),...
        'UAV','FontSize',20)
    %Annotate where the UGV is
    text(x_ugv(i),y_ugv(i),z_ugv(i),'UGV','FontSize',20)
    %Annotate with Time, Distance and FOV angle
    time_text = strcat('Time : ', num2str(t_uav(i)), ' s');
    %Calculate the distance from UGV to UAV
    distance = sqrt((x_uav(i)-x_ugv(i))^2 + ...
        (y_uav(i)-y_ugv(i))^2 + ...
        (z_uav(i)-z_ugv(i))^2);
    distance_text = strcat('Distance :  ', num2str(distance), ' m');
    %Calculate the required AOV with and without GPS RTK
    AOV_angle(i) = 2*atand(max([x_err_uav,y_err_uav,z_err_uav])/distance);
    AOV_angle_rtk(i) = 2*atand(max([x_uav_err_rtk(i),x_uav_err_rtk(i),z_err_uav])/distance);
    %Plot the required angle of view cone; this is based on whatever error
    %is set above
    AOV_text = strcat('AOV Angle : ',num2str(AOV_angle(i)),' deg');
    plot_text = {time_text,distance_text, AOV_text};
    %Place this text here
    text(-1*max(x_uav),0.1*max(y_uav),0.1*max(z_uav), ...
        plot_text)
    %Annotate with UAV and UGV X,Y,Z,Bearing
    veh_text = {'UAV State' , ...
        strcat('X :',num2str(x_uav(i)), 'm'),...
        strcat('Y :',num2str(y_uav(i)), 'm'),...
        strcat('Z :',num2str(z_uav(i)), 'm'),...
        strcat('\Phi :',num2str(heading_uav(i)), 'deg'),...
        'UGV State' , ...
        strcat('X :',num2str(x_ugv(i)), 'm'),...
        strcat('Y :',num2str(y_ugv(i)), 'm'),...
        strcat('Z :',num2str(z_ugv(i)), 'm'),...
        strcat('\Phi :',num2str(heading_ugv(i)), 'deg')};
    %Plot text
    text(1.2*max(x_uav),1.2*max(y_uav),1.2*max(z_uav), ...
        veh_text)
    
    %Create cone of Camera Angle of View, doesn't account for latency
    [Cone,EndPlate1,EndPlate2] = cone([x_ugv(i),y_ugv(i),z_ugv(i)], ... %Location of UGV
        [x_uav(i),y_uav(i),z_uav(i)],...%Location of UAV
        [0,tand(45/2)*distance], ... %Max radius
        100,'r',0,1);
    
    %% Plot headings
    
    %Plot vector showing heading of UAV
    vectarrow([x_uav(i +latency/timestep),...
        y_uav(i+ latency/timestep), ...
        z_uav(i+ latency/timestep)],...
        [x_uav(i+ latency/timestep)+10*cosd(heading_uav(i+ latency/timestep)),...
        y_uav(i+ latency/timestep)+10*sind(heading_uav(i+ latency/timestep)),...
        z_uav(i+ latency/timestep)])
    
    
    %Plot vector showing heading of UAV with error
    vectarrow([x_uav(i+ latency/timestep),...
        y_uav(i + latency/timestep),...
        z_uav(i+ latency/timestep)],...
        [x_uav(i+ latency/timestep)+10*cosd(heading_uav(i+ latency/timestep)),...
        y_uav(i+ latency/timestep)+10*sind(heading_uav_err(i+ latency/timestep)),...
        z_uav(i+ latency/timestep)])
    
    %Plot vector showing heading of UGV
    vectarrow([x_ugv(i),y_ugv(i),z_ugv(i)],...
        [x_ugv(i)+10*cosd(heading_ugv(i)),y_ugv(i)+10*sind(heading_ugv(i)),z_ugv(i)])
    
    
    
    
    %% Label the plot
    title('Flight Model with FOV Cone','FontSize',20)
    xlabel('X Position [m]','FontSize',20)
    ylabel('Y Position [m]','FontSize',20)
    zlabel('Altitude [m]','FontSize',20)
    
    legend('UAV Position Error','Required AOV',...
        'Camera AOV','Location','best')
    set(gca,'Fontsize',20)
    
    pause(.01)
    
    hold off
end

%% Post flight analysis
%Plot required Angles of View
figure
hold on
plot(t_uav, AOV_angle,'Linewidth',2)
plot(t_uav, AOV_angle_rtk,'g','Linewidth',2)
plot([0,max(t_uav)],[45,45],'r--','Linewidth',2)
title('Simulated Required Angle of View at Range of 10 m','Fontsize',20)
xlabel('Time of Simulation [s]','Fontsize',20)
ylabel('Instantaneous Angle of View [deg]','Fontsize',20)
legend('Simulated with Standalone GPS Error','Simulated with RTK GPS Error',...
    'Camera Angle of View','Location','best')
set(gca,'Fontsize',20)
grid on
ylim([0,max(AOV_angle)])
hold off


