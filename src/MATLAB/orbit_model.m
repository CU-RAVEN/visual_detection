function [t, x, y, z, heading] = orbit_model( ... 
    orbit_radius, ...   %The radius of the UAV orbit [m]
    orbit_altitude, ... %The altitude of the orbit [m]
    orbit_speed, ...    %The flight speed [m/s]
    flight_time,...     %The total time of flight [s]
    dt...               %Timestep [s]
     )  %End of input arguments
 
    %Create time array t
    t = linspace(0,flight_time,flight_time/dt+1);
    %How long is one revolution
    one_rev_dist = pi*2*orbit_radius;
    %Period [s]
    orbit_period = one_rev_dist/orbit_speed;
    %Determine location in orbit 
    theta = t/orbit_period*360;
    %State the x location
    x = orbit_radius*cosd(theta);
    %Determine the y location
    y = orbit_radius*sind(theta);
    %State that the altitude will remain the same
    z = linspace(orbit_altitude, orbit_altitude,flight_time/dt+1);
    %Bearing is perpendicular
    heading = mod(theta+90,360);    
    
    
end

