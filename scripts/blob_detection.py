#!/usr/bin/env python

"""
Author: Ian Loefgren
Created: 10/4/2017
Last Modified: 10/4/2017

Blob detection proof of concept using OpenCv3's blob detection libraries.
Subscribes to a ROS topic for image data.
"""

import rospy
import cv2
import numpy as np
from cv_bridge import CvBridge,CvBridgeError
from sensor_msgs.msg import Image, CameraInfo

class Detector(object):
    def __init__(self,capture_mode='video'):

        self.capture_mode = capture_mode

        # set up blob detector
        self.params = cv2.SimpleBlobDetector_Params()
        
        # thresholding
        self.params.minThreshold = 0
        self.params.maxThreshold = 127

        # area filtering
        self.params.filterByArea = True
        self.params.minArea = 100
        self.params.maxArea = 10000

        # convexity filtering
        self.params.filterByConvexity = False
        self.params.minConvexity = 0
        self.params.maxConvexity = 0.5

        # intertia-ratio filtering (how oblong the blob is, low = oblong)
        self.params.filterByInertia = False
        self.params.maxInertiaRatio = 0.6 

        # create detector instance
        self.detector = cv2.SimpleBlobDetector_create(self.params)

        # initialize node, set up subscriber to cam info, cv bridge instance
        if capture_mode == 'video':
            rospy.init_node('blob_detector')
            self.sub = rospy.Subscriber('/cv_camera/image_raw',Image,self.callback)
            self.bridge = CvBridge()
            rospy.spin()
        else:
            self.load_image('../images/img5.jpg')

    def blob_detect(self,img):
        """
        Detect objects
        """
         # Blur image to remove noise
        frame=cv2.GaussianBlur(img, (3, 3), 0)
        # hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

        # define color range
        # grey_min = (0,0,0)
        # grey_max = (0,0,35)
        blue_min = (100,50,60)
        blue_max = (300,150,100)

        # mask image with color range
        # mask = cv2.inRange(hsv,blue_min,blue_max)

        # find key points in image using detector
        keypoints = self.detector.detect(img)
        # compute keypoints
        # cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS ensures the size of the circle corresponds to the size of blob
        im_with_keypoints = cv2.drawKeypoints(img, keypoints, np.array([]), (0,0,255), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

        # show detections
        cv2.imshow('detections',im_with_keypoints)
        if self.capture_mode == 'video':
            cv2.waitKey(1)
        else:
            cv2.waitKey(0)

    def callback(self,msg):
        """
        ROS subscriber callback to handle new image data as it is published
        """
        # convert ROS image msg to OpenCv image
        img = self.bridge.imgmsg_to_cv2(msg,"bgr8")

        thresh = self.threshold(img)
        self.blob_detect(thresh)

    def load_image(self,filename):
        """
        Load test image from file
        """
        img = cv2.imread(filename)
        self.blob_detect(img)
        # self.contour_detect(img)
        thresh = self.threshold(img)
        self.blob_detect(thresh)

    def contour_detect(self,img):
        """
        Contour detection
        """
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        gray = cv2.bilateralFilter(gray, 11, 17, 17)
        edged = cv2.Canny(gray, 30, 200)

        # find contours in the edged image, keep only the largest
        # ones, and initialize our screen contour
        (image,cnts,_) = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(cnts, key = cv2.contourArea, reverse = True)[:10]
        screenCnt = None


        cv2.drawContours(img, cnts, -1, (0, 255, 0), 3)

        cv2.imshow('contours',img)
        cv2.waitKey(0)

    def masking(self,img):
        pass

    def threshold(self,img):
        gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
        ret,thresh = cv2.threshold(gray,150,255,cv2.THRESH_TRUNC)
        return thresh
        # cv2.imshow('threshold img',thresh)
        # if self.capture_mode == 'video':
        #     cv2.waitKey(1)
        # else:
        #     cv2.waitKey(0)

if __name__ == "__main__":
    Detector('still')


