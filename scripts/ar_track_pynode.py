

import rospy
import cv2
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CameraInfo

rospy.init_node('video_pub')
pub = rospy.Publisher('/video',Image,queue_size=10)
info_pub = rospy.Publisher('/video_info',CameraInfo,queue_size=10)

bridge = CvBridge()

r = rospy.Rate(10) # Hz
cap = cv2.VideoCapture(0)

info_msg = CameraInfo()

while not rospy.is_shutdown():
    ret, frame = cap.read()
    try:
        # rgb = cv2.cvtColor(frame,cv2.COLOR_BGR2RGB)
        msg =   bridge.cv2_to_imgmsg(frame,encoding ="bgr8")
        pub.publish(msg)
    
    except CvBridgeError as e:
        print(e)

    r.sleep()